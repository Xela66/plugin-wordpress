<?php

if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

require_once plugin_dir_path( __FILE__ ) . "/service/Database_Service.php";

class Liste extends WP_List_Table {
	private $dal;

	public function __construct( $args = array() ) {
		parent::__construct( [
			'singular' => __( "Client" ),
			'plural'   => __( "Clients" )
		] );

		$this->dal = new Database_Service();
	}


	public function prepare_items() {
		$columns  = $this->get_columns();
		$hidden   = $this->get_hidden_columns();
		$sortable = $this->get_sortable_columns();

		// Configuration de la Pagination
		$perPage     = $this->get_items_per_page( 'client_per_page', 10 );
		$currentPage = $this->get_pagenum();

		// Récupération des données
		$data      = $this->dal->findAll();
		$totalPage = count( $data );

		// Tri des colonnes
		usort( $data, array( &$this, 'usort_reorder' ) );
		$paginationData = array_slice( $data, ( ( $currentPage - 1 ) * $perPage ), $perPage );

		$this->set_pagination_args( [
			'total_items' => $totalPage,
			'per_page'    => $perPage
		] );

		// Construction
		$this->_column_headers = [ $columns, $hidden, $sortable ];
		$this->items           = $paginationData;

	}

	public function column_default( $item, $column_name ) {
		switch ( $column_name ) {
			case 'id':
			case 'nom':
			case 'prenom':
			case 'email':
			case 'telephone':
				return $item->$column_name;
				break;
			case 'fidelite':
				return $item->$column_name == 0 ? "Non" : "Oui"; // Transforme la valeur Boolean en String
				break;
			default:
				return print_r( $item, true );
		}
	}

	public function usort_reorder( $a, $b ) {
		$orderBy = ( ! empty( $_GET['orderby'] ) ) ? $_GET['orderby'] : 'id';
		$order   = ( ! empty( $_GET['order'] ) ) ? $_GET['order'] : 'desc';
		$result  = strcmp( $a->$orderBy, $b->$orderBy );

		return ( $order === 'asc' ) ? $result : - $result;
	}

	public function get_columns() {
		$columns = [
			'nom'       => 'Nom',
			'prenom'    => 'Prenom',
			'email'     => 'Email',
			'telephone' => 'Téléphone',
			'fidelite'  => 'Abonnement',
		];

		return $columns;
	}

	public function get_hidden_columns() {
		return [];
	}

	public function get_sortable_columns() {
		return $sortable = [
			'id'        => [ 'id', true ],
			'nom'       => [ 'nom', true ],
			'prenom'    => [ 'prenom', false ],
			'email'     => [ 'email', false ],
			'telephone' => [ 'telephone', false ],
			'fidelite'  => [ 'fidelite', false ]
		];
	}


}
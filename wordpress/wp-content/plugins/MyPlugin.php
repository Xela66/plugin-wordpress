<?php
/*
 * Plugin Name: My Plugin
 * Description: My WP Plugin
 * Author: Xela66
 * Version: 1.0.0
 */

require_once plugin_dir_path( __FILE__ ) . "widget/Widget_Random_Photo.php";
require_once plugin_dir_path( __FILE__ ) . "service/Database_Service.php";
require_once plugin_dir_path( __FILE__ ) . "Liste.php";

class Coding {
	public function __construct() {

		// Activation du Plug-in : Création d'une table
		register_activation_hook( __FILE__, array( 'Database_Service', 'create_db' ) );

		// Désactivation du Plug-in : Vidange de la table
		register_deactivation_hook( __FILE__, array( 'Database_Service', 'empty_db' ) );

//		// Suppression du Plug-in : Suppression de la table /!\
//		register_uninstall_hook(__File__, array('Database_Service', 'drop_db'));

		add_action( 'widgets_init', function () {
			register_widget( 'Widget_Random_Photo' );
		} );

		add_action( 'admin_menu', [ $this, 'add_menu_client' ] );

	}

	public function add_menu_client() {
		add_menu_page( "Plug-in Coding", "Coding Plug-in",
			"manage_options", "codingClient",
			array( $this, "mes_clients" ), "dashicons-groups", 40 );
		add_submenu_page( 'codingClient', 'Liste des clients', "Liste",
			"manage_options", 'codingClient', [ $this, "mes_clients" ] );
		add_submenu_page( 'codingClient', 'Ajouter un client', "Ajouter",
			"manage_options", 'addClient', [ $this, "mes_clients" ] );
	}


	public function mes_clients() {
		$db = new Database_Service();
		echo "<h2>" . get_admin_page_title() . "</h2>";
		if ( $_REQUEST['page'] == 'codingClient' || $_POST['send'] == 'ok' || $_POST['action'] == 'del' ) {

			if ( isset( $_POST['send'] ) && $_POST['send'] == 'ok' ) {
				$db->save_client();
			}
			if ( isset( $_POST['action'] ) && $_POST['action'] == 'del' ) {
				$db->deleteClient( $_POST['id'] );
			}

			// Liste des clients
			echo "<form method='post' class='wrap'>";
			$table = new Liste();
			$table->prepare_items();
			echo $table->display();
			echo "</form>";


//			echo "<table class='table-border'>";
//			foreach ( $db->findAll() as $client ) {
//				echo "<tr>";
//				echo "<td>" . $client->nom . "</td>";
//				echo "<td>" . $client->prenom . "</td>";
//				echo "<td>" . $client->email . "</td>";
//				echo "<td>" . $client->telephone . "</td>";
//				echo "<td>" . ( ( $client->fidelite == 0 ) ? "Non" : "Oui" ) . "</td>";
//				echo "<td><form method='post'>".
//				     "<input type='hidden' name='action' value='del' />".
//				     "<input type='hidden' name='id' value='" . $client->id ."' />".
//				     "<input type='submit' value='del' />".
//				     "</form></td>";
//				echo "</tr>";
//			}
//			echo "</table>";

		} else { // Fomulaire de sauvegarde

			echo "<form class='wrap' method='post'>" .
			     "<input type='hidden' name='send' value='ok' />" .
			     "<div><label for='nom'>Nom : </label>" .
			     "<input type='text' id='nom' name='nom' class='widefat' required/></div>" .
			     "<div><label for='prenom'>Prenom : </label>" .
			     "<input type='text' id='prenom' name='prenom' class='widefat' required/></div>" .
			     "<div><label for='email'>Email : </label>" .
			     "<input type='text' id='email' name='email' class='widefat' required/></div>" .
			     "<div><label for='Telephone'>Telephone : </label>" .
			     "<input type='text' id='telephone' name='telephone' class='widefat' required/></div>" .
			     "<div><label>Abonnement : </label><br>" .
			     "<select name='fidelite' id='fidelite' class='widefat' required>Abonnement :" .
			     "<option value='false'/> Non</option>" .
			     "<option value='true'/> Oui</option>" .
			     "</select></div>" .
			     "<br/>" .
			     "<br/>" .
			     "<br/>" .
			     "<hr/>" .
			     "<div><input type='submit' value='Enregistrer' class='widefat button button-primary'/></div></form>";
		}
	}
}

new Coding();
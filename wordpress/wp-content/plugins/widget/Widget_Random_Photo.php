<?php

class Widget_Random_Photo extends WP_Widget {
	public function __construct() {
		$widget_ops = array(
			'className'                   => 'Random_Photo',
			'description'                 => __( "Randomise les photos" ),
			'customize_selective_refresh' => true
		);
		parent::__construct( 'photos', __( 'Random Photos' ), $widget_ops );
	}

	// formulaire de config en back office
	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array(
			"query" => "",
			"nbr"   => "",
			"cle"   => "",
		) );

		?>
        <p>
            <label for="<?php echo $this->get_field_id( 'query' ); ?>">Votre recherche</label>
            <input type="text"
                   id="<?php echo $this->get_field_id( 'query' ); ?>"
                   name="<?php echo $this->get_field_name( 'query' ); ?>"
                   value="<?php echo esc_attr( $instance['query'] ); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'nbr' ); ?>">Nombre de photos</label>
            <input type="text" id="<?php echo $this->get_field_id( 'nbr' ); ?>"
                   name="<?php echo $this->get_field_name( 'nbr' ); ?>"
                   value="<?php echo esc_attr( $instance['nbr'] ); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'cle' ); ?>">Entrer votre clé Unsplash</label>
            <input type="text" id="<?php echo $this->get_field_id( 'cle' ); ?>"
                   name="<?php echo $this->get_field_name( 'cle' ); ?>"
                   value="<?php echo esc_attr( $instance['cle'] ); ?>">
        </p>
		<?php
	}

	public function update( $new_instance, $old_instance ) {
		$instance          = $old_instance;
		$instance['query'] = sanitize_text_field( $new_instance['query'] );
		$instance['nbr']   = sanitize_text_field( $new_instance['nbr'] );
		$instance['cle']   = sanitize_text_field( $new_instance['cle'] );

		return $instance;
	}

	public function widget( $args, $instance ) {
		$title = "Photos Widget";

		// Nombre de photo mini.
		( $instance['nbr'] !== 0 ) ? $nbr = $instance['nbr'] : $nbr = 1;

		// URL d'API
		$url = "https://api.unsplash.com/search/photos?query=" . $instance['query'] .
		       "&per_page=" . $nbr;

		$argsCle = [
			'headers' => [
				'Authorization' => 'Client-ID ' . $instance['cle']
			]
		];

		// Appel à l'API
		$request = wp_remote_get( $url, $argsCle );
		if ( is_wp_error( $request ) ) {
			return false;
		}
		$body = wp_remote_retrieve_body( $request );
		$data = json_decode( $body, true );
		//var_dump($data);

		echo $args['before_widget'];
		echo $args['before_title'] . $title . $args['after_title'];
		echo "<div class='photo'>";
		if ( ! empty( $data ) ) {
			for ( $i = 0; $i < $nbr; $i ++ ) {
				echo "<p>" . $data['results'][ $i ]['id'] . "</p>";
				echo "<img src='" . $data['results'][ $i ]['urls']['thumb'] . "'/>";
			}
		}
		echo "</div>";
		echo $args['after_widget'];

		return '';
	}

}
<?php

class Database_Service {

	public function __construct() {
	}

	public static function create_db() {
		global $wpdb;
		// Création d'une table
		$wpdb->query( "CREATE TABLE IF NOT EXISTS " .
		              "{$wpdb->prefix}client ( " .  // table=> wp_client
		              "id INT AUTO_INCREMENT PRIMARY KEY, " .
		              "nom VARCHAR(150) NOT NULL, " .
		              "prenom VARCHAR(200) NOT NULL, " .
		              "email VARCHAR(250) NOT NULL, " .
		              "telephone VARCHAR(50) NOT NULL, " .
		              "fidelite BOOLEAN DEFAULT false " .
		              ");" );

		// Comptes les lignes existantes
		$count = $wpdb->get_var( "SELECT count(*) FROM {$wpdb->prefix}client;" );

		// Ajout d'une valeur si la table est vide
		if ( $count == 0 ) {
			$wpdb->insert( "{$wpdb->prefix}client", [
				'nom'       => "Collado",
				'prenom'    => "Alexandre",
				'email'     => "alexandre.collado@outlook.fr",
				'telephone' => "0651136355",
				'fidelite'  => true
			] );
		}

	}

	public static function empty_db() {
		global $wpdb;
		$wpdb->query( "TRUNCATE {$wpdb->prefix}client;" );
	}

	public static function drop_db() {
		global $wpdb;
		$wpdb->query( "DROP TABLE {$wpdb->prefix}client;" );
	}

	// La liste des clients
	public function findAll() {
		global $wpdb;
		$q = $wpdb->get_results( "SELECT `id`, `nom`,`prenom`,`email`,`telephone`,`fidelite`,
       									(CASE `fidelite`
       									    WHEN 1 THEN 'Oui'
       									    ELSE 'Non'
                                        END)
       									FROM {$wpdb->prefix}client;" );

		return $q;
	}

	public function save_client() {
		global $wpdb;
		// Insérer les valeurs dans une table
		$valeurs = [
			'nom'       => $_POST['nom'],
			'prenom'    => $_POST['prenom'],
			'email'     => $_POST['email'],
			'telephone' => $_POST['telephone'],
			'fidelite'  => filter_var( $_POST['fidelite'], FILTER_VALIDATE_BOOLEAN ),
		];

		// Vérification
		$row =
			$wpdb->get_row( "SELECT id FROM {$wpdb->prefix}client WHERE email='{$valeurs['email']}';" );
		if ( is_null( $row ) ) {
			$wpdb->insert( "{$wpdb->prefix}client", $valeurs );
		}
	}

	// Suppression d'une valeur
	public function deleteClient( $ids ) {
		global $wpdb;

		if ( ! is_array( $ids ) ) {
			$ids = array( $ids );
		}

		$wpdb->query( "DELETE FROM {$wpdb->prefix}client " .
		              "WHERE id IN (" . implode( ',', $ids ) . ")" );
	}

}
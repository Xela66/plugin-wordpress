<div>
    <h3>
        <a href="<?php the_permalink(); ?>"><?php the_title() ?></a>
    </h3>
	<?php if ( 'post' == get_post_type() ) : ?>
        <div class="blog-postmeta">
            <div class="post-date"><?php echo get_the_date() ?></div>
        </div>
	<?php
	endif;
	?>
</div>
<div class="entry-summary">
	<?php the_excerpt(); ?>
    <a href="<?php the_permalink(); ?>"><?php esc_html_e( "lire plus &rarr;" ); ?></a>
</div>

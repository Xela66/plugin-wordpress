<div class="col-sm-3 col-sm-offset-1 blog-sidebar">
    <div class="sidebar-module sidebar-module-inset">
        <!--		--><?php //if ( is_single() ) : ?>
        <h5>A propos</h5>
        <p>
			<?php
			the_author_meta( 'description' );
			?>
        </p>
        <h5>Autres articles</h5>
        <ol class="list-unstyled">
			<?php
			$currentId    = get_the_ID();
			$author       = get_the_author();
			$auteur_posts = new WP_Query( array( 'author_login' => $author ) ); // Récupère les auteurs de la bdd
			//  Boucle sur les post des auteurs
			while ( $auteur_posts->have_posts() ): $auteur_posts->the_post();
				if ( get_the_ID() !== $currentId ) {
					?>
                    <!-- Affiche le lien, le titre et l'auteur de l'article -->
                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?> par <?php the_author(); ?></a></li>
					<?php
				}
			endwhile;
			?>
        </ol>
        <h5>Archives</h5>
        <ol class="list-unstyled">
			<?php wp_get_archives( 'type=monthly' ); ?>
        </ol>
    </div>
</div>



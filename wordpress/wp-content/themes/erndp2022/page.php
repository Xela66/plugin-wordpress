<?php get_header(); ?>
<main class="p-3">
    <header class="page-header">
		<?php // affichage les articles de la categorie
		the_title( "<h4>", "</h4>" );
		?>
    </header>
    <div class="row">
        <div class="col-sm-8 blog-main ">
			<?php
			if ( have_posts() ) : while ( have_posts() ): the_post();
				get_template_part( 'content', 'page', get_post_format() );
			endwhile;
			endif;
			?>
        </div>
		<?php get_sidebar(); ?>

    </div>
</main>
<?php get_footer(); ?>


<?php


// Enregistrement du menu de navigation
function register_menu() {
	register_nav_menus(
		array(
			'menu-header' => __( 'Menu Header' ),
			'menu-footer' => __( 'Menu Footer' )
		)
	);
}

add_action( 'init', 'register_menu' ); // Ajout du menu dans l'interface admin

function registerWidget() {
	register_sidebar( [
		'name'          => 'Sidebar',
		'description'   => __( "ma zone de widget" ),
		'id'            => 'mySidebar',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	] );

	register_widget( 'Random_Photo' );
}

add_action( 'widgets_init', 'registerWidget' );

/**
 * widget d'affichage de photo aléatoire
 */
class Random_Photo extends WP_Widget {
	// surcharger le constructeur
	public function __construct() {
		$widget_ops = array(
			'className'                   => 'Random_Photo',
			'description'                 => __( "Randomise les photos" ),
			'customize_selective_refresh' => true
		);
		parent::__construct( 'photos', __( 'Random Photos' ), $widget_ops );
	}

	// formulaire de config en back office
	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array(
			"query" => "",
			"nbr"   => "",
			"cle"   => "",
		) );

		?>
        <p>
            <label for="<?php echo $this->get_field_id( 'query' ); ?>">Mot de recherche</label>
            <input type="text"
                   id="<?php echo $this->get_field_id( 'query' ); ?>"
                   name="<?php echo $this->get_field_name( 'query' ); ?>"
                   value="<?php echo esc_attr( $instance['query'] ); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'nbr' ); ?>">Nombre de photos</label>
            <input type="text" id="<?php echo $this->get_field_id( 'nbr' ); ?>"
                   name="<?php echo $this->get_field_name( 'nbr' ); ?>"
                   value="<?php echo esc_attr( $instance['nbr'] ); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'cle' ); ?>">Cle Unsplash</label>
            <input type="text" id="<?php echo $this->get_field_id( 'cle' ); ?>"
                   name="<?php echo $this->get_field_name( 'cle' ); ?>"
                   value="<?php echo esc_attr( $instance['cle'] ); ?>">
        </p>
		<?php
	}

	public function update( $new_instance, $old_instance ) {
		$instance          = $old_instance;
		$instance['query'] = sanitize_text_field( $new_instance['query'] );
		$instance['nbr']   = sanitize_text_field( $new_instance['nbr'] );
		$instance['cle']   = sanitize_text_field( $new_instance['cle'] );

		return $instance;
	}

	public function widget( $args, $instance ) {
		$title = "Photos";

		// nbre de photo minimum
		( $instance['nbr'] !== 0 ) ? $nbr = $instance['nbr'] : $nbr = 1;

		// construire l'url d'appel
		$url = "https://api.unsplash.com/search/photos?query=" . $instance['query'] .
		       "&per_page=" . $nbr;

		$argsCle = [
			'headers' => [
				'Authorization' => 'Client-ID ' . $instance['cle']
			]
		];

		// appel à  l'api
		$request = wp_remote_get( $url, $argsCle );
		if ( is_wp_error( $request ) ) {
			return false;
		}
		$body = wp_remote_retrieve_body( $request );
		$data = json_decode( $body, true );
		//var_dump($data);

		echo $args['before_widget'];
		echo $args['before_title'] . $title . $args['after_title'];
		echo "<div class='photo'>";
		if ( ! empty( $data ) ) {
			for ( $i = 0; $i < $nbr; $i ++ ) {
				echo "<p>" . $data['results'][ $i ]['id'] . "</p>";
				echo "<img src='" . $data['results'][ $i ]['urls']['thumb'] . "'/>";
			}
		}
		echo "</div>";
		echo $args['after_widget'];

		return '';
	}

}


/**
 * Nouvelle classe Walker
 */
class My_walker extends Walker_Nav_Menu {
	public function start_el(
		&$output, $data_object, $depth = 0,
		$args = null, $current_object_id = 0
	) {
		$menu_item = $data_object;

		$title       = $menu_item->title;
		$description = $menu_item->description;
		$permalink   = $menu_item->url;

		$output .= "<li class='nav-item list-unstyled'>";
		$output .= "<a href='" . $permalink . "' class='nav-link text-light'>";

		$output .= $title;
		$output .= "</a>";
	}

//    public function end_el(&$output, $data_object, $depth = 0, $args = null)
//    {
//         $output .= "</li>";
//    }
}

/**
 * short code
 */
function monShortCode( $atts ) {
	return "<strong>Mes reductions :</strong>";
}

add_shortcode( 'myShort', 'monShortCode' );

function monShortPromo( $atts ) {
	$a = shortcode_atts( [ 'percent' => 10 ], $atts );

	return "<strong>Reduction de {$a['percent']}%</strong>";
}

add_shortcode( 'promo', 'monShortPromo' );
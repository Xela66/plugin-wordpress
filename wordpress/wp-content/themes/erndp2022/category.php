<?php get_header() ?>
    <div class="page-container">
        <div class="content-wrap">
            <main class="p-3">
                <header class="page-header">
					<?php //  affichage des articles de la categorie
					the_archive_title( "<h4>", "</h4>" );
					the_archive_description( "<em>", "</em>" );
					?>
                </header>
                <div class="row">
                    <div class="col-sm-8 blog-main">
						<?php
						if ( have_posts() ) : while ( have_posts() ): the_post();
							get_template_part( 'content', 'category', get_post_format() );
						endwhile;
						endif;
						?>
                    </div>
					<?php get_sidebar(); ?>
                </div>
            </main>
        </div>
    </div>
<?php get_footer() ?>
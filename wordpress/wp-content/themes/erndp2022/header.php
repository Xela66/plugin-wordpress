<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css"
          integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx"
          crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() . "/style.css"; ?>">
    <title>Mon Thème WordPress</title>
</head>
<body>
<header class="p-3 bg-dark text-white text-center">
    <h2>
        <a href="<?php echo get_bloginfo( 'wpurl' ); ?>" class="text-light">
			<?php echo get_bloginfo( 'name' ); ?>
        </a>
    </h2><br/>
    <em class="blog-description text-white"><?php echo get_bloginfo( 'description' ) ?></em>
    <!-- element du menu -->
    <!--    <nav class="navbar navbar-expand-lg navbar-light">-->
	<?php wp_nav_menu( [
		"theme_location"  => "menu-header",
		"container"       => "nav",
		"container_class" => "navbar navbar-expand-lg navbar-light",
		"menu_class"      => "navbar justify-content-around d-flex w-100",
		"menu_id"         => " ",
		"walker"          => new My_walker,
	] ); ?>
    </nav>
</header>
<?php

<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * Localized language
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** Database username */
define( 'DB_USER', 'wordpress' );

/** Database password */
define( 'DB_PASSWORD', 'wordpress' );

/** Database hostname */
define( 'DB_HOST', 'database' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          ')5~}8azwd1Oml7wqiYr4du3t~~:!M>O}IpBEt]cB:FVL6Jo,P96@onP%P7BL&1}}' );
define( 'SECURE_AUTH_KEY',   ']QaP5A5><Y5m>.rR%+i=P2mL?8t}=]xZEB!4w_ERap;YvrZIS7+#=F>K+*=WrR`|' );
define( 'LOGGED_IN_KEY',     'Nu!];|N[;*FvY7CGStT1i?5aP*tb.GQT<.6d~iMS{z/~Vfg6/D<BS8>78.t$VTkH' );
define( 'NONCE_KEY',         '4lc1:eto:>Sfshp3qbj0.53$evA6]7 vF^b+h1viCdjsfLp(Ry<R!~8-7Qj[AM,O' );
define( 'AUTH_SALT',         '7wS6 q]g8TKRH*]3.8sisk~FGMBO]Q$iT%VEoPBau&tY0459Ir=!N7/0))?T#R4U' );
define( 'SECURE_AUTH_SALT',  '-~{.6YeH3zS?5GO4oj5SV+5)3H_lmg@hq8nEa.k>>={73$qALOqH]LT4#~S;HSNx' );
define( 'LOGGED_IN_SALT',    'OMgACU*V#Jbo@`ote>`)=zCN7~u!HxAD](.E<^=O]*n&m$:|p{--vRXhrR:oH6l`' );
define( 'NONCE_SALT',        ';!e;-~bpIZOR/eU8.1ZtS$B|U8VbKWCt4SMP,oCH7kg92M0I9_sF@=sKU977,N}1' );
define( 'WP_CACHE_KEY_SALT', 'N`drEvWJWacY YH:S@Wou%Q!gdmrR*{jo/rCaya6%V7jRCUuUI/]f%.[02E><*Ei' );


/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );


/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
